var _ = require('underscore');

var env = {
  es6: true,
  node: true
};

var parserOptions = {
  ecmaVersion: 6,
  sourceType: 'module'
};

var airbnbBestPractices = require('eslint-config-airbnb-base/rules/best-practices');
// var airbnbErrors = require('eslint-config-airbnb-base/rules/errors');
var airbnbES6 = require('eslint-config-airbnb-base/rules/es6');
// var airbnbNode = require('eslint-config-airbnb-base/rules/node');
// var airbnbStrict = require('eslint-config-airbnb-base/rules/strict');
var airbnbStyle = require('eslint-config-airbnb-base/rules/style');
var airbnbVariables = require('eslint-config-airbnb-base/rules/variables');

var rules = _.extend({},
    airbnbBestPractices.rules,
    // airbnbErrors.rules,
    airbnbES6.rules,
    // airbnbNode.rules,
    // airbnbStrict.rules,
    airbnbStyle.rules,
    airbnbVariables.rules,
    {
        "arrow-body-style": ["off", "as-needed"],
        "arrow-parens": ["off"],
        "comma-dangle": 0,
        "consistent-return": "off",
        "class-methods-use-this": "off", // it does not require the use of this keyword on react components
        "dot-notation": "off", // confirm if we'll change brackets for dot notation
        "eol-last": ["off"],
        "default-case": "off", // no default case required
        "eqeqeq": ["warn"],
        "import/prefer-default-export": [0],
        "import/no-extraneous-dependencies": "off",
        "import/first": [0],
        "import/extensions": "off",
        "jsx-a11y/label-has-for": "off",
        "jsx-a11y/no-static-element-interactions": "off",
        "jsx-a11y/href-no-hash": "off", // allow hash on href
        "jsx-a11y/heading-has-content": "off", //allow JSX heading shortcut
        "max-len": ["error", 120],
        "newline-per-chained-call": ["error", { "ignoreChainWithDepth": 6 }],
        "no-unused-vars": 0,
        "no-use-before-define": "off",
        "no-unused-expressions": ["error", { "allowShortCircuit": true, "allowTernary": true }],
        "no-mixed-operators": "off",
        "no-console": ["error", { allow: ["info", "error"] }],
        "no-alert": "off",
        "new-cap": ["error", { "newIsCap": false, "capIsNew": false } ],
        "no-undef": "off",
        "no-underscore-dangle": "off",
        "no-param-reassign": "off",
        "no-return-assign": "warn", // allow assignments on return
        "no-var": 0,
        "no-else-return": 0,
        "no-lonely-if": "off",
        "no-case-declarations": "off",
        "no-shadow": ["off"],
        "object-curly-spacing": ["error", "always", { "objectsInObjects": true }],
        "prefer-const": "off",
        "prefer-spread": "off", // allow .apply()
        "radix": ["error", "as-needed"]
    }
);

module.exports = {
  env: env,
  parserOptions: parserOptions,
  rules: rules
};
